package resizer.util;

import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;

import javax.imageio.ImageIO;
import java.util.ResourceBundle;

public final class AppConstants {

    public static final String HOME_DIR = System.getProperty("user.home");
    public static final ResourceBundle MESSAGES = ResourceBundle.getBundle("msgs");
    public static final Rectangle2D SCREEN_BOUNDS = Screen.getPrimary().getVisualBounds();
    public static final double DEFAULT_FONT_SIZE = SCREEN_BOUNDS.getMaxX() / 114.285714286;
    public static final String[] SUPPORTED_IMAGE_FORMATS = ImageIO.getReaderFileSuffixes();

    private AppConstants() {
    }
}
