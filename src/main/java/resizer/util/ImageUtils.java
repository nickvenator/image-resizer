package resizer.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;

import static resizer.util.AppConstants.SUPPORTED_IMAGE_FORMATS;

public final class ImageUtils {

    private ImageUtils() {
    }

    private static final Logger logger = LogManager.getLogger();

    /**
     * Used to resize an image with keeping  aspect ratio or without it. Returns scaled image.
     *
     * @param image  a <code>File</code> to be resized.
     * @param width  of the scaled image, if equals -1 keep aspect ratio using the width value.
     * @param height of the scaled image,if equals -1 keep aspect ratio using the height value.
     * @return scaled image.
     */
    public static BufferedImage resize(File image, int width, int height) {

        BufferedImage originalImage = readImage(image);

        if (width == -1) {
            double ratio = (double) height / (double) originalImage.getHeight();
            height = (int) Math.round(originalImage.getHeight() * ratio);
            width = (int) Math.round(originalImage.getWidth() * ratio);
        } else if (height == -1) {
            double ratio = (double) width / (double) originalImage.getWidth();
            width = (int) Math.round(originalImage.getWidth() * ratio);
            height = (int) Math.round(originalImage.getHeight() * ratio);
        }

        int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();

        BufferedImage scaledImage = new BufferedImage(width, height, type);
        Graphics2D g = scaledImage.createGraphics();
        g.drawImage(originalImage, 0, 0, width, height, null);
        g.dispose();

        return scaledImage;
    }

    /**
     * Writes an image to a <code>File</code>. If there is already a <code>File</code> present, its contents are
     * discarded.
     *
     * @param image      a <code>RenderedImage</code> to be written.
     * @param formatName a <code>String</code> containing the name of the format.
     * @param outputFile a <code>File</code> to be written to.
     * @throws IllegalArgumentException        if any parameter is <code>null</code>.
     * @throws UnsupportedImageFormatException if <code>formatName</code>is <code>null</code> or
     *                                         <code>String</code> is empty.
     */
    public static void saveImage(RenderedImage image, String formatName, File outputFile) {
        if (isCorrectImageFormat(formatName)) {
            try {
                ImageIO.write(image, formatName, outputFile);
            } catch (IOException e) {
                logger.error("Can't save image!", e);
            }
        } else {
            throw new UnsupportedImageFormatException("Image format '.*" + formatName + " is unsupported");
        }
    }

    /**
     * Returns a <code>BufferedImage</code> containing the decoded contents of the image, or <code>null</code>.
     *
     * @param image a <code>File</code> to read from.
     * @return a <code>BufferedImage</code> containing the decoded contents of the image, or <code>null</code>.
     */
    private static BufferedImage readImage(File image) {
        BufferedImage img = null;
        try {
            img = ImageIO.read(image);
        } catch (IOException e) {
            logger.error("Can't readImage input file!", e);
        }
        return img;
    }

    /**
     * Returns the file extension for a given file name or empty string if file does not have extension.
     *
     * @param fileName it's a name of passed file or full path to the file.
     * @return the file extension for a given file name or empty string if file does not have extension.
     */
    public static String getExtension(String fileName) {
        String fileExtension;
        if (fileName.contains(".")) {
            fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
            logger.info("File extension is '*." + fileExtension);
        } else {
            fileExtension = "";
            logger.info("File does not have extension or it is a folder");
        }
        return fileExtension;
    }

    /**
     * Returns <code>true</code> if image format is supported and <code>false</code> otherwise.
     *
     * @param imageFormat a <code>String</code> containing a image format.
     * @return <code>true</code> if image format is supported and <code>false</code> otherwise.
     */
    private static boolean isCorrectImageFormat(String imageFormat) {
        boolean isCorrectImageFormat = false;
        if (imageFormat != null && !imageFormat.isEmpty()) {
            for (String format : SUPPORTED_IMAGE_FORMATS) {
                if (format.equalsIgnoreCase(imageFormat)) {
                    isCorrectImageFormat = true;
                    break;
                }
            }
        }
        return isCorrectImageFormat;
    }
}
