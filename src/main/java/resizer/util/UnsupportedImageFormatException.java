package resizer.util;

/**
 * Unchecked exception thrown to indicate that a method has been passed an illegal or
 * inappropriate image extension
 *
 * @author Nick Alekseev
 * @since 1.7
 */

public class UnsupportedImageFormatException extends IllegalArgumentException {

    /**
     * Constructs an <code>UnsupportedImageFormatException</code> with no detail message.
     */
    public UnsupportedImageFormatException() {
    }

    /**
     * Constructs an <code>IllegalArgumentException</code> with the specified detail message.
     *
     * @param s the detail message.
     */
    public UnsupportedImageFormatException(String s) {
        super(s);
    }
}
