package resizer;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;

import static resizer.util.AppConstants.*;

public class App extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        URL mainViewUrl = getClass().getClassLoader().getResource("view/view.fxml");
        assert mainViewUrl != null;
        Parent rootNode = FXMLLoader.load(mainViewUrl, MESSAGES);

        Scene scene = new Scene(rootNode);
        scene.getStylesheets().add("css/style.css");
        scene.getRoot().setStyle("-fx-font-size:" + DEFAULT_FONT_SIZE + ";");


        stage.setTitle(MESSAGES.getString("imageResizer"));
        stage.setWidth(SCREEN_BOUNDS.getMaxX() * 0.4);
        stage.setHeight(SCREEN_BOUNDS.getMaxY() * 0.3);
        stage.setResizable(false);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
