package resizer;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import resizer.util.AppConstants;
import resizer.util.ImageUtils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static resizer.util.AppConstants.MESSAGES;
import static resizer.util.AppConstants.SUPPORTED_IMAGE_FORMATS;

public class AppController {

    private static final Logger logger = LogManager.getLogger();

    private static File initialDirectory;

    @FXML
    private Button fileChooserBtn;
    @FXML
    private Button folderChooserBtn;
    @FXML
    private TextField destinationPath;
    @FXML
    private Button destinationChooserBtn;
    @FXML
    private TextField imageWidth;
    @FXML
    private TextField imageHeight;
    @FXML
    private Button resizeBtn;
    @FXML
    private ProgressBar progressBar;
    @FXML
    ListView<Path> imagesPathsList;


    @FXML
    public void initialize() {
        initialDirectory = new File(AppConstants.HOME_DIR);
        initNodesSizes();

        fileChooserBtn.setOnMouseClicked(event -> chooseFiles());
        folderChooserBtn.setOnMouseClicked(event -> chooseFolder());
        destinationChooserBtn.setOnMouseClicked(event -> chooseDestination());
        imageWidth.setOnKeyTyped(this::textFieldChangeListener);
        imageHeight.setOnKeyTyped(this::textFieldChangeListener);
        resizeBtn.setOnMouseClicked(event -> startResize());
    }

    private void initNodesSizes() {
        Platform.runLater(() -> {
            imagesPathsList.setPrefWidth(imagesPathsList.getScene().getWidth() * 0.6);
            imagesPathsList.setPrefHeight(imagesPathsList.getScene().getHeight() * 0.4);
            destinationPath.setPrefWidth(destinationPath.getScene().getWidth() * 0.6);
            imageWidth.setPrefWidth(imageWidth.getScene().getWidth() * 0.115);
            imageHeight.setPrefWidth(imageHeight.getScene().getWidth() * 0.115);
            progressBar.setPrefWidth(progressBar.getScene().getWidth());
        });
    }

    /**
     * Shows a new file open dialog.
     * You can choose file which has next extensions: *.jpg, *.bmp, *.gif, *.png, *.jpeg, *.wbmp.
     */
    private void chooseFiles() {
        logger.info("Button '" + fileChooserBtn.getId() + "' has been clicked");

        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter(MESSAGES.getString("imageFiles"), getImageExtensions());

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(MESSAGES.getString("selectImages"));
        fileChooser.getExtensionFilters().add(filter);
        fileChooser.setInitialDirectory(initialDirectory);

        List<File> images = fileChooser.showOpenMultipleDialog(fileChooserBtn.getScene().getWindow());
        if (images != null) {
            ObservableList<Path> paths = imagesPathsList.getItems();
            paths.clear();
            paths.addAll(images.stream().map(File::toPath).collect(Collectors.toList()));
            imagesPathsList.getStyleClass().remove("error");
            initialDirectory = images.get(images.size() - 1).getParentFile().getAbsoluteFile();
            logger.info("Images have been selected");
        } else {
            logger.info("Files haven't been selected");
        }
    }

    /**
     * Shows a folder chooser dialog.
     * Chooses images paths in selected folder which have next extensions: *.jpg, *.bmp, *.gif, *.png, *.jpeg, *.wbmp.
     */
    private void chooseFolder() {
        logger.info("Button '" + folderChooserBtn.getId() + "' has been clicked");

        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle(MESSAGES.getString("selectFolder"));
        directoryChooser.setInitialDirectory(initialDirectory);

        File directory = directoryChooser.showDialog(folderChooserBtn.getScene().getWindow());
        if (directory != null) {
            try {
                ObservableList<Path> paths = imagesPathsList.getItems();
                paths.clear();
                Files.walk(Paths.get(directory.toURI())).forEach(filePath -> {
                    if (Files.isRegularFile(filePath) && isImage(filePath.toString())) {
                        paths.add(filePath);
                    }
                });
                imagesPathsList.getStyleClass().remove("error");
                initialDirectory = directory.getParentFile().getAbsoluteFile();
                logger.info("Folder " + directory.getAbsolutePath() + " has been selected");
            } catch (IOException e) {
                logger.error("Cannot read files from directory {" + directory.getAbsolutePath() + "}");
            }
        } else {
            logger.info("Folder hasn't been selected");
        }
    }

    /**
     * Shows a folder chooser dialog.
     * Selected destination where will be saved resized images
     */
    private void chooseDestination() {
        logger.info("Button '" + destinationChooserBtn.getId() + "' has been clicked");

        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle(MESSAGES.getString("selectDestination"));
        directoryChooser.setInitialDirectory(initialDirectory);

        File directory = directoryChooser.showDialog(destinationChooserBtn.getScene().getWindow());
        if (directory != null) {
            destinationPath.setText(directory.getAbsolutePath());
            destinationPath.getStyleClass().remove("error");
            initialDirectory = directory.getParentFile().getAbsoluteFile();
            logger.info("Folder " + destinationPath.getText() + " has been selected");
        } else {
            destinationPath.setText(MESSAGES.getString("folderIsNotSelected"));
            logger.info("Folder hasn't been selected");
        }
    }

    /**
     * Returns list of supported images extensions.
     *
     * @return list of supported images extensions.
     */
    private List<String> getImageExtensions() {
        List<String> imageExtensions = new ArrayList<>();
        for (String imageExtension : SUPPORTED_IMAGE_FORMATS) {
            imageExtensions.add("*." + imageExtension);
        }
        return imageExtensions;
    }

    /**
     * Returns {@code true} if the checked file has content type "imagePath" and {@code false} otherwise.
     *
     * @param path - the pathname of checked file.
     * @return {@code true} if the checked file has content type "imagePath" and {@code false} otherwise.
     */
    private boolean isImage(String path) {
        String mimeType = URLConnection.guessContentTypeFromName(path);
        if (mimeType != null) {
            String type = mimeType.split("/")[0];
            return type.equals("image");
        }
        return false;
    }

    private void startResize() {
        // Create a Runnable
        Runnable task = this::resizeTask;
        // Run the task in a background thread
        Thread backgroundThread = new Thread(task);
        // Terminate the running thread if the application exits
        backgroundThread.setDaemon(true);
        // Start the thread
        backgroundThread.start();
    }

    private void resizeTask() {
        if (checkRequiredFields()) {
            logger.info("Resizing has been started");

            progressBar.setVisible(true);

            double progressStep = 1.0 / imagesPathsList.getItems().size();
            for (Path imagePath : imagesPathsList.getItems()) {
                resizeImage(imagePath);
                Platform.runLater(() -> progressBar.setProgress(progressBar.getProgress() + progressStep));
            }

            progressBar.setVisible(false);
            progressBar.setProgress(0.0);

            logger.info("Resizing has been finished");
        }
    }

    private boolean checkRequiredFields() {

        boolean isFilledRight = true;

        if (imagesPathsList.getItems().isEmpty()) {
            addStyleClass(imagesPathsList, "error");
            logger.info("Files haven't been selected");
            isFilledRight = false;
        }
        if (destinationPath.getText().isEmpty()) {
            addStyleClass(destinationPath, "error");
            logger.info("Folder hasn't been selected");
            isFilledRight = false;
        }
        if (imageWidth.getText().isEmpty() && imageHeight.getText().isEmpty()) {
            addStyleClass(imageWidth, "error");
            addStyleClass(imageHeight, "error");
            logger.info("Image width and height have not been set");
            isFilledRight = false;
        }

        return isFilledRight;
    }

    private void addStyleClass(Control node, String styleClass) {
        for (String style : node.getStyleClass()) {
            if (style.equalsIgnoreCase(styleClass)) {
                return;
            }
        }
        node.getStyleClass().add(styleClass);
    }

    private void resizeImage(Path imagePath) {
        int width = imageWidth.getText().isEmpty() || Math.signum(Integer.valueOf(imageWidth.getText())) < 0 ? -1 : Integer.valueOf(imageWidth.getText());
        int height = imageHeight.getText().isEmpty() || Math.signum(Integer.valueOf(imageHeight.getText())) < 0 ? -1 : Integer.valueOf(imageHeight.getText());

        BufferedImage image = ImageUtils.resize(imagePath.toFile(), width, height);

        String imageFormatName = ImageUtils.getExtension(imagePath.toFile().getName());
        String destination = destinationPath.getText() + File.separator + imagePath.getFileName();
        ImageUtils.saveImage(image, imageFormatName, new File(destination));
    }

    private void textFieldChangeListener(Event event) {
        imageWidth.getStyleClass().remove("error");
        imageHeight.getStyleClass().remove("error");

        TextField textField = (TextField) event.getSource();
        if (textField.getText().length() > 3) {
            String text = textField.getText();
            textField.clear();
            Platform.runLater(() -> {
                textField.setText(text);
                textField.positionCaret(text.length());
            });
        }
    }
}
